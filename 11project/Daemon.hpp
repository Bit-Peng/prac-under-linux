#pragma once
#include <iostream>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

using namespace std;

const char * root="/";//路径
const char *dev_null="/dev/null";
void Deamon(bool ischild,bool isclose)
{
    //1,忽略可能引起程序异常退出的信号
    signal(SIGCHLD,SIG_IGN);//为什么有两个，这里有遗忘的部分
    signal(SIGPIPE,SIG_IGN);
    if(fork()>0)
    {
        exit(0);
    }
    //设置让自己成为一个新的会话，后面的代码其实是子程序再走

    //这部分其实是守护进程部分
    setsid();
    if(ischild)
    {
        chdir(root);
    }
    if(isclose)
    {
        close(0);
        close(1);
        close(2);
    }
    else
    {
        int fd=open(dev_null,O_RDWR);
        if(fd>0)
        {
            dup2(fd,0);
            dup2(fd,1);
            dup2(fd,2);
            close(fd);
        }
    }

}