#pragma once
#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;

class InetAddr
{
public:
    InetAddr(struct sockaddr_in &addr):_addr(addr)
    {
        _port=ntohs(_addr.sin_port);
        //_ip=inet_ntoa(_addr.sin_addr);//
        char ipbuffer[64];
        inet_ntop(AF_INET,&addr.sin_addr,ipbuffer,sizeof(ipbuffer));//1,网络转本机，2,4字节ip,字符串风格ip
        _ip=ipbuffer;
    }
    string Ip(){return _ip;};
    uint16_t Port(){return _port;};
    string PrintDebug()
    {
        string info =_ip;
        info+=":";
        info+=to_string(_port);
        return info;
    }
    const struct sockaddr_in& Getaddr()
    {
        return _addr;
    }
    bool operator==(const InetAddr& addr)
    {
        return this->_ip==addr._ip&&this->_port==addr._port;
    }
    ~InetAddr(){}
private:
    string _ip;
    uint16_t _port;
    struct sockaddr_in _addr;
};