#pragma once

#include <iostream>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <functional>
#include <unordered_map>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include "Log.hpp"
#include "nocopy.hpp"
#include "Comm.hpp"
#include "InetAddr.hpp"
#include "ThreadPool.hpp"

using namespace std;

// class ThreadData
// {
// public:
//     InetAddr _addr;
//     ThreadData(int sock,TcpServer* ptr,struct sockaddr_in &peer):
//     sockfd(sock),svr_ptr(ptr),_addr(peer)
//     {}
//     int SockFd()
//     {
//         return sockfd;
//     }
//     TcpServer* GetServer()
//     {
//         return svr_ptr;
//     }
//     ~ThreadData(){}

// private:
//     int sockfd;
//     TcpServer* svr_ptr;
// };

const static int default_backlog = 3;
using task_t = function<void()>;
using callback_t = std::function<void(int, InetAddr &)>;

class TcpServer : public nocopy
{
public:
    TcpServer(uint16_t port) : _port(port), _isrunning(false)
    {
    }
    void Init() // 准备工作
    {
        // 1,创建socket，file,fd，本质是文件
        _listensock = socket(AF_INET, SOCK_STREAM, 0);
        if (_listensock < 0)
        {
            lg.LogMessage(Fatal, "create socket error,errno code:%d.error string :%s\n", errno, strerror(errno));
            exit(Fatal);
        }
        lg.LogMessage(Debug, "create socket success,sockfd:%d\n", _listensock);
        // 固定写法，解决一些少量的bind失败问题
        int opt = 1;
        setsockopt(_listensock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
        // 填充本体网络信息并bind
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = htonl(INADDR_ANY);
        // bind
        if (bind(_listensock, CONV(&local), sizeof(local)) != 0)
        {
            lg.LogMessage(Fatal, "bind socket error,errno code %d,error string %s\n", errno, strerror(errno));
            exit(Bind_Err);
        }
        lg.LogMessage(Debug, "bind success,sockfd:%d\n", _listensock);

        if (listen(_listensock, default_backlog) != 0)
        {
            lg.LogMessage(Fatal, "listen socket error,error code is%d,error string :%s\n", errno, strerror(errno));
        }
        lg.LogMessage(Debug, "listen success,sockfd:%d\n", _listensock);

        ThreadNS::ThreadPool<task_t>::GetInstance()->Start();
        funcs.insert(make_pair("defaultService", std::bind(&TcpServer::DefaultService, this, placeholders::_1, placeholders::_2)));//在init时就已经启动了        
    }
    void DefaultService(int sockfd, InetAddr &addr)
    {
        (void)addr;
        string defaultstring = "|";
        for (auto func : funcs)
        {
            defaultstring += func.first;
            defaultstring += "|";
        }
        cout << defaultstring << endl;
        write(sockfd, defaultstring.c_str(), sizeof(defaultstring));
    }
    void Service(int sockfd, InetAddr &addr)
    {
        char buffer[1024];
        while (true)
        {
            ssize_t n = read(sockfd, buffer, sizeof(buffer) - 1);
            if (n > 0)
            {
                buffer[n] = 0;
                cout << addr.PrintDebug() << "#" << buffer << endl;
            }
            else if (n == 0)
            {
                lg.LogMessage(Info, "client quit....");
                break;
            }
            else
            {
                lg.LogMessage(Error, "read sock error,errno code :%d,error string is%s\n", errno, strerror(errno));
            }
        }
    }
    // static void * HandlerRequest(void * args)
    // {
    //     pthread_detach(pthread_self());
    //     ThreadData *td=static_cast<ThreadData*>(args);
    //     td->GetServer()->Service(td->SockFd(),td->_addr);
    //     delete td;
    //     return nullptr;
    // }
    void Start() // 对比tcp和udp如何不同
    {
        _isrunning = true;
        signal(SIGCHLD, SIG_IGN);
        while (_isrunning)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            int sockfd = accept(_listensock, CONV(&peer), &len);//这里应该是等待接受
            if (sockfd < 0)
            {
                lg.LogMessage(Warning, "accept socket error,errno code:%d,error string :%s\n", errno, strerror(errno));
                continue;
            }
            lg.LogMessage(Debug, "accept success,get a new sockfd:%d\n", sockfd);
            // 多线程版本
            //  ThreadData*td =new ThreadData(sockfd,this,peer);
            //  pthread_t tid;
            //  pthread_create(&tid,nullptr,HandlerRequest,td);//这里等会再看一看

            task_t t = std::bind(&TcpServer::Routine, this, sockfd, InetAddr(peer)); // 绑定参数要设置相同
            ThreadNS::ThreadPool<task_t>::GetInstance()->Push(t);                    // 得到一个进程池对象，直接push一个任务
        }
    }
    string Read(int sockfd)
    {
        char type[1024];
        ssize_t n = read(sockfd, type, sizeof(type));
        if (n > 0)
        {

            type[n] = 0;
            lg.LogMessage(Info, "read a message is %s\n",type);

        }
        else if (n == 0)
        {
            lg.LogMessage(Info, "client quit...\n");
        }
        else
        {
            lg.LogMessage(Error, "read socket error,errno code is :%d,error sting :%s\n", errno, strerror(errno));
        }
        return type;
    }
    void Routine(int sockfd, InetAddr addr)
    {
                cout<<"******************************"<<endl;

        funcs["defaultService"](sockfd, addr); // 这里是调用

        string type = Read(sockfd);

        lg.LogMessage(Debug, "%s select %s\n", addr.PrintDebug().c_str(), type.c_str());
        // 接收到ping，这里是返回决策
        funcs[type](sockfd, addr);
        close(sockfd);
    }
    void RegisterFunc(const string &name,callback_t func)
    {
        funcs[name]=func;//绑定,插入
    }
    ~TcpServer()
    {

    }

private:
    uint16_t _port;
    int _listensock;
    bool _isrunning;
    std::unordered_map<string, callback_t> funcs;//回调函数和他的名字
};
