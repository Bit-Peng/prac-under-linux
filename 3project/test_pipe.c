#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

// int main()
// {
//     int fds[2];
//     char buffer[128];
//     if(pipe(fds)==-1)
//     {
//         perror("make pipe error");
//         exit(2);
//     }
//     scanf("%s",buffer);
//     int len=strlen(buffer);

//     write(fds[1],buffer,len);
//     memset(buffer,0x00,sizeof(buffer));//重新置零
//     //读取
//     if(len=read(fds[0],buffer,len)==-1)
//     {
//         perror("文件读取失败\n");
//         exit(1);
//     }
//     printf("%s\n",buffer);

//     return 0;
// }

//父子进程共享管道
//linux下一切皆文件

// int main()
// {
//     //创建管道
//     int fds[2];
//     if(pipe(fds)==-1)
//     {
//         perror("pipe open error");
//         exit(1);
//     }
//     pid_t id;
//     id=fork();
//     if(id==-1)
//     {
//         perror("fork fail");
//         exit(2);
//     }
//     if(id==0)
//     {
//         //子进程
//         close(fds[1]);
//         char buffer[128];
//         read(fds[0],buffer,128);
//         printf("%s\n",buffer);
//         close(fds[0]);
//     }
//     else
//     {
//         close(fds[0]);
//         write(fds[1],"heihei",6);
//         close(fds[1]);
//     }
//     return 0;
// }
