#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;

#define Convert(addrptr) ((struct sockaddr *)addrptr)
// backlog最大链接支持数

const static int defaultsockfd = 1;
const int backlog = 5;
enum
{
    SocketError = 1,
    BindError,
    ListenError,
};
class Socket
{
public:
    virtual ~Socket() {}
    virtual void CreateSocketOrDie() = 0; // 设置为纯虚函数.
    virtual void BindSocketOrDie(uint16_t port) = 0;
    virtual void ListenSocketOrDie(int backlog) = 0;
    virtual Socket *AcceptConnection(string *peerip, uint16_t *peerport) = 0;
    virtual bool ConnectServer(string &server, uint16_t serverport) = 0;
    virtual int GetSockFd() = 0;
    virtual void SetSockFd(int sockfd) = 0; // 这个的功能是?
    virtual void CloseSocket() = 0;
    virtual bool Recv(string *buffer, int size) = 0;
    virtual void Send(string &send_str) = 0;
    // 想添加什么新的功能就自己添加

    void BuildListenSocketMethod(uint16_t port, int backlog)//这个是服务端，只需要绑定
    {
        CreateSocketOrDie();
        BindSocketOrDie(port);
        ListenSocketOrDie(backlog); // 监听不能超过backlog个链接
    }
    bool BuildConnectSocketMethon(string &serverip, uint16_t serverport)//这个是客户端
    {
        CreateSocketOrDie();
        return ConnectServer(serverip, serverport);//是否连接成功
    }
    void BuildNormalSocketMethod(int sockfd)
    {
        SetSockFd(sockfd);
    }
};
class TcpSocket : public Socket
{
public:
    TcpSocket(int sockfd = defaultsockfd) : _sockfd(sockfd)
    {
    }
    ~TcpSocket()
    {
    }
    void CreateSocketOrDie() override
    {
        _sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (_sockfd < 0)
        {
            exit(SocketError); // 设置退出码
        }
    }
    void BindSocketOrDie(uint16_t port) override
    {
        struct sockaddr_in local;
        memset(&local, 0, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_addr.s_addr = INADDR_ANY;
        local.sin_port = htons(port);
        int n = bind(_sockfd, Convert(&local), sizeof(local));
        if (n < 0)
        {
            exit(BindError);
        }
    }
    void ListenSocketOrDie(int backlog) override
    {
        int n = listen(_sockfd, backlog);
        if (n < 0)
        {
            exit(ListenError);
        }
    }
    Socket *AcceptConnection(std::string *peerip, uint16_t *peerport) override
    {
        struct sockaddr_in peer;
        socklen_t len = sizeof(peer);
        int newsockfd = accept(_sockfd, Convert(&peer), &len);
        if (newsockfd < 0)
        {
            return nullptr;
        }
        *peerport = ntohs(peer.sin_port);
        *peerip = inet_ntoa(peer.sin_addr);
        Socket *s = new TcpSocket(newsockfd); // 要将其基类全部虚函数实现才能new出来
        return s;
    }
    bool ConnectServer(string &serverip, uint16_t serverport) override
    {
        //所以说这个就没有bind
        struct sockaddr_in server;
        memset(&server, 0, sizeof(server));
        server.sin_family = AF_INET;
        server.sin_addr.s_addr = inet_addr(serverip.c_str());
        server.sin_port = htons(serverport);
        int n = connect(_sockfd, Convert(&server), sizeof(server));
        if (n == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    int GetSockFd() override
    {
        return _sockfd;
    }
    void SetSockFd(int sockfd) override
    {
        _sockfd = sockfd;
    }
    void CloseSocket() override
    {
        if (_sockfd > defaultsockfd)
        {
            close(_sockfd);
        }
    }
    bool Recv(string *buffer, int size) override
    {
        // 这里没有规定使用的协议，单纯只是收发字节流
        char inbuffer[size];
        ssize_t n = recv(_sockfd, inbuffer, size - 1, 0);
        if (n > 0)
        {
            inbuffer[n] = 0;
            *buffer += inbuffer;
            return true;
        }
        else if (n == 0)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
    void Send(string &send_str) override
    {
        send(_sockfd, send_str.c_str(), send_str.size(), 0);
    }

private:
    int _sockfd;
};
