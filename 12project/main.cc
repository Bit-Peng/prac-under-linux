#pragma once
#include "Socket.hpp"
#include "TcpServer.hpp"
#include <iostream>
#include <unistd.h>
#include <memory>

using namespace std;
//两参数，作为服务端
int main(int argc,char*argv[])
{
    uint16_t port=stoi(argv[1]);
    cout<<"问题???"<<endl;

    unique_ptr<TcpServer> usvr(new TcpServer(port));
    usvr->Loop();
    return 0;
}