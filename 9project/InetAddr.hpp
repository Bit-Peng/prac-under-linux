#pragma once
#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

using namespace std;
class InetAddr
{
public:
    InetAddr(struct sockaddr_in& addr):_addr(addr)
    {
        _port=ntohs(_addr.sin_port);
        _ip=inet_ntoa(_addr.sin_addr);
    }
    string Ip()
    {
        return _ip;
    }
    uint16_t Port()
    {
        return _port;
    }
    string PrintDebug()
    {
        string info=_ip;
        info+=':';
        info+=std::to_string(_port);
        return info;
    }
    ~InetAddr()
    {

    }
private:
    string _ip;
    uint16_t _port;//这里由于是协议规定
    struct sockaddr_in _addr;
};