#include "Comm.hpp"
#include <memory>
#include "UdpSever.hpp"
#include "Usage.hpp"

//./udp_server 8888
int main(int argc,char *argv[])
{
    if(argc!=2)
    {
        Usage(argv[0]);
    }
    //ip=argv[1]
    uint16_t port=stoi(argv[1]);
    unique_ptr<UdpServer> usvr=make_unique<UdpServer>(port);
    usvr->Init();//这里是指针形式
    usvr->Start();

    return 0;
}
