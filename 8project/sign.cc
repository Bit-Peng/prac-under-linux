#pragma once

// 掌握信号篇章的知识
#include <iostream>
#include <unistd.h>
#include <signal.h>
using namespace std;

void handler(int sign)
{
    cout<<"catch a signal: "<<sign<<endl;
}

int main()
{
    // while(1)
    // {
    //     printf("heihei\n");
    //     sleep(1);
    // }
    signal(2,handler);
    int a=0;
    while(1)
    {
        a++;
        sleep(1);
        if(a==10)
        {
            raise(9);//自己给自己发送信号
        }
    }

    return 0;
}

