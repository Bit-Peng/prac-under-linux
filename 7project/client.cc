#include "common.h"
#include <cstring>
#include <string>
int main()
{
    // 这里是写端
    int m = open("filename", O_WRONLY | 0666);
    if (m < 0)
    {
        perror("open error");
        exit(1);
    }
    while (true)
    {
        string buffer;
        cin >> buffer;
        ssize_t n = write(m, buffer.c_str(), strlen(buffer.c_str()));
        if (n < 0)
        {
            break;
        }
    }
    return 0;
}