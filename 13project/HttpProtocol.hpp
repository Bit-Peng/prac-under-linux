#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

const std::string HttpSep = "\r\n";

class HttpRequest
{
public:
    HttpRequest() : _req_blank(HttpSep)
    {
    }
    bool GetLine(string &str, string *line)
    {
        auto pos = str.find(HttpSep); // 分割点
        if (pos == string::npos)
            return false;
        *line = str.substr(0, pos); // 字符串截取
        str.erase(0, pos + HttpSep.size());
        return true;
    }
    void Deserialize(string &request) // 这个函数是做什么的
    {
        string line;
        bool ok = GetLine(request, &line); // 经过这个函数之后，req是内容，line是包头
        if (!ok)
            return;
        _req_line = line;
        while (true)
        {
            if (ok && line.empty())
            {
                _req_content = request;
                break;
            }
            else if (ok && !line.empty())
            {
                _req_header.push_back(line);//这个是做什么？
            }
            else
            {
                break;
            }
        }
    }
    void DebugHttp()
    {
        cout<<"_req_line"<<_req_line<<endl;
        for(auto &line:_req_header)
        {
            cout<<"------->"<<line<<endl;
        }
        //接下来是分解的报文
        cout<<"_req_content"<<_req_content<<endl;
    }
    ~HttpRequest()
    {}
private:
    string _req_line;
    vector<string> _req_header;
    string _req_blank;
    string _req_content;
};