#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdarg>
#include <ctime>
#include <unistd.h>
#include <fcntl.h>
using namespace std;

enum
{
    Debug = 0,
    Info,
    Warning,
    Error,
    Fatal
};

enum
{
    Screen = 10,
    OneFile,
    ClassFile
};
string LevelToString(int level)
{
    switch (level)
    {
    case Debug:
        return "Debug";
    case Info:
        return "Info";
    case Warning:
        return "Warning";
    case Error:
        return "Error";
    case Fatal:
        return "Fatal";
    }
}
const int defaultstyle = Screen;
const string default_filename = "log."; // 这是做什么的？
const string logdir = "log";

class Log
{
public:
    Log() : style(defaultstyle), filename(default_filename)
    {
        mkdir(logdir.c_str(), 0775);
    }
    void Enable(int sty)//使用哪种记录方式可供选择
    {
        style = sty;
    }
    string TimeStampExlocalTime() // 记录日志的时间
    {
        time_t currtime = time(nullptr);
        struct tm *curr = localtime(&currtime);
        char time_buffer[128];
        snprintf(time_buffer, sizeof(time_buffer), "%d-%d-%d %d:%d:%d", curr->tm_year + 1900, curr->tm_mon + 1, curr->tm_mday, curr->tm_hour, curr->tm_min, curr->tm_sec);
        return time_buffer;
    }
    void WriteLogToOneFile(const string &logname, const string &message)
    {
        umask(0);
        int fd = open(logname.c_str(), O_CREAT | O_WRONLY | O_APPEND, 0666);
        if (fd < 0)
            return;
        write(fd, message.c_str(), message.size());
        close(fd);

        ofstream out(logname);
        if (!out.is_open())
        {
            return ;
        }
        out.write(message.c_str(),message.size());
        out.close();
    }
    void WriteLogToClassFile(const string &levelstr,const string &message)//份文件输出
    {
        string logname=logdir;
        logname +='/';
        logname+=filename;
        logname+=levelstr;
        WriteLogToOneFile(logname,message);
    }
    void WriteLog(const string &levelstr,const string &message)
    {
        switch(style)
        {
            case Screen:
                cout<<message<<endl;
                break;
            case OneFile:
                WriteLogToOneFile("all",message);//这里的all
                break;

            case ClassFile:
                WriteLogToClassFile(levelstr,message);
                break;
            default:
                break;
        }
    }


    void LogMessage(int level,const char*format,...)//就像C语言中的printf一样，变长,外界将内容传入，该函数接受并合并。
    {
        char leftbuffer[1024];
        string levelstr=LevelToString(level);//根据整型值返回一个字符串
        string currentime=TimeStampExlocalTime();//这个是时间
        string idstr=to_string(getpid());

        //可变参数提取
        char rightbuffer[1024];
        va_list args;
        //args指向了可变参数部分
        va_start(args,format);
        vsnprintf(rightbuffer,sizeof(rightbuffer),format,args);
        va_end(args);
        snprintf(leftbuffer,sizeof(leftbuffer),"[%s][%s][%s]",levelstr.c_str(),currentime.c_str(),idstr.c_str());
        string loginfo =leftbuffer;
        loginfo +=rightbuffer;
        WriteLog(levelstr,loginfo);
    }


private:
    int style;
    string filename;
};

Log lg;
class Conf
{
public:
    Conf()
    {
        lg.Enable(Screen);//默认显示屏吗
    }
    ~Conf()
    {

    }
};
Conf conf;