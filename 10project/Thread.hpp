#pragma once

#include <iostream>
#include <string>
#include <functional>
#include <pthread.h>

using namespace std;

//typedef function<void()> func_t;//定义一种函数类型
//同样有这种方法
template<class T>
using func_t=function<void(T&)>;

template<class T>
class Thread
{
public:
    Thread(const string &threadname,func_t<T> func,T &data)
    :_tid(0),_threadname(threadname),_isrunning(false),_func(func),_data(data)
    {

    }
    static void *ThreadRoutine(void* args)//运行
    {
        Thread* ts=static_cast<Thread*>(args);//这里是如何回调的？
        ts->_func(ts->_data);
        return nullptr;
    }
    bool Start()
    {
        int n=pthread_create(&_tid,nullptr,ThreadRoutine,this);//创建之后会直接运行吗？
        if(n==0)
        {
            _isrunning=true;
            return true;
        }
        return false;
    }
    bool Join()
    {
        if(!_isrunning)return true;
        int n=pthread_join(_tid,nullptr);//线程结束，等待，淡然也可以detach
        if(n==0)
        {
            _isrunning=false;
            return true;
        }
        return false;
    }
    string ThreadName()
    {
        return _threadname;
    }
    bool IsRunning()
    {
        return _isrunning;
    }
    ~Thread()
    {}
private:
    pthread_t _tid;
    string _threadname;
    bool _isrunning;
    func_t<T> _func;
    T _data;
};
