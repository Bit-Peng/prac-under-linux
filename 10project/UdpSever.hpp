// #pragma once
// #include <iostream>
// #include <string>
// #include <cerrno>
// #include <cstring>
// #include <unistd.h>
// #include <strings.h>
// #include <sys/types.h>
// #include <sys/socket.h>
// #include <netinet/in.h>
// #include <arpa/inet.h>
// #include "Log.hpp"
// #include "InetAddr.hpp"
// #include "Comm.hpp"
// #include "nocopy.hpp"
// #include "ThreadPool.hpp"
// #include <functional>
// using namespace std;

// const static uint16_t defaultport=8888;
// const static int defaultfd=-1;
// const static int defaultsize=1024;
// using task_t = function<void()>;


// class UdpServer:public nocopy
// {
// public:
//     UdpServer(uint16_t port=defaultport)
//     :_port(port),_sockfd(defaultfd)
//     {
//         pthread_mutex_init(&_user_mutex,nullptr);
//     }
 
//     void Init()
//     {
//         //创建socket，就是创建了文件细节
//         _sockfd=socket(AF_INET,SOCK_DGRAM,0);
//         if(_sockfd<0)
//         {
//             lg.LogMessage(Fatal,"socket err,%d :%s\n",errno,strerror(errno));
//             exit(Socket_Err);
//         }
//         lg.LogMessage(Info,"socket success,socketfd:%d",_sockfd);
//         //绑定上网信息
//         struct sockaddr_in local;
//         bzero(&local,sizeof(local));
//         local.sin_family=AF_INET;
//         local.sin_port=htons(_port);
//         local.sin_addr.s_addr=INADDR_ANY;

//         //结构体填完，接下来设置到内核
//         int n=bind(_sockfd,(struct sockaddr*)&local,sizeof(local));
//         if(n!=0)
//         {
//             lg.LogMessage(Fatal,"bind error,%d:%s\n",errno,sizeof(local));
//             exit(Bind_Err);
//         }
//         ThreadPool<task_t>::GetInstance()->Start();
//     }
//     void AddOnlineUser(InetAddr addr)
//     {
//         LockGuard lockguard(&_user_mutex);
//         for(auto &user:_online_user)
//         {
//             if(addr == user)
//             {
//                 return ;
//             }
//         }
//         _online_user.push_back(addr);
//         lg.LogMessage(Debug,"server send message to %s:%d,message:%s\n",addr.Ip().c_str(),addr.Port());
//     }
//     void Route(int sock,const string &message)//
//     {
//         LockGuard lockguard(&_user_mutex);//枷锁和释放都在这里
//         for(auto &user:_online_user)
//         {
//             sendto(sock,message.c_str(),message.size(),0,(struct sockaddr *)&user.GetAddr(),sizeof(user.GetAddr()));
//             lg.LogMessage(Debug,"server send message to %s:%d,message:%s\n",user.Ip(),user.Port(),message.c_str());
//         }
//     }
//     void Start()
//     {
//         char buffer[defaultsize];
//         for(;;)
//         {
//             struct sockaddr_in peer;
//             socklen_t len=sizeof(peer);
//             ssize_t n=recvfrom(_sockfd,buffer,sizeof(buffer)-1,0,(struct sockaddr*)&peer,&len);
//             if(n>0)
//             {
//                 InetAddr addr(peer);
//                 AddOnlineUser(addr);
//                 buffer[n]=0;
//                 string message ="[";
//                 message+=addr.Ip();
//                 message+=":";
//                 message+=to_string(addr.Port());
//                 message+="#]";
//                 message+=buffer;
//                 task_t task=bind(&UdpServer::Route,this,_sockfd,message);//后边跟随的是传递的函数传参
//                 ThreadPool<task_t>::GetInstance()->Push(task);
//                 cout<<buffer<<endl;
//                 string response = buffer;
//                 sendto(_sockfd,response.c_str(),response.size(),0,(struct sockaddr*)&peer,len);
//             }
//         }
//     }
//     ~UdpServer()
//     {
//         //释放锁
//         pthread_mutex_destroy(&_user_mutex);
//     }
// private:
//     uint16_t _port;
//     int _sockfd;
//     vector<InetAddr> _online_user;
//     pthread_mutex_t _user_mutex;
// };

#pragma once

#include <iostream>
#include <string>
#include <cerrno>
#include <cstring>
#include <unistd.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <functional>
#include <pthread.h>
// #include <mutex>
// #include <condition_variable>
#include "nocopy.hpp"
#include "Log.hpp"
#include "Comm.hpp"
#include "InetAddr.hpp"
#include "ThreadPool.hpp"

const static uint16_t defaultport = 8888;
const static int defaultfd = -1;
const static int defaultsize = 1024;
using task_t = std::function<void()>;

// using cb_t = std::function<std::string(std::string)>; // 定义了一个函数类型

// 聚焦在IO上
class UdpServer : public nocopy
{
public:
    // UdpServer(cb_t OnMessage, uint16_t port = defaultport)
    //     : _port(port), _sockfd(defaultfd), _OnMessage(OnMessage)
    UdpServer(uint16_t port = defaultport) : _port(port), _sockfd(defaultfd)
    {
        pthread_mutex_init(&_user_mutex, nullptr);
    }
    void Init()
    {
        // 1. 创建socket，就是创建了文件细节
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (_sockfd < 0)
        {
            lg.LogMessage(Fatal, "socket errr, %d : %s\n", errno, strerror(errno));
            exit(Socket_Err);
        }

        lg.LogMessage(Info, "socket success, sockfd: %d\n", _sockfd);

        // 2. 绑定，指定网络信息
        struct sockaddr_in local;
        bzero(&local, sizeof(local)); // memset
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = INADDR_ANY; // 0

        // local.sin_addr.s_addr = inet_addr(_ip.c_str()); // 1. 4字节IP 2. 变成网络序列

        // 结构体填完，设置到内核中了吗？？没有
        int n = ::bind(_sockfd, (struct sockaddr *)&local, sizeof(local));
        if (n != 0)
        {
            lg.LogMessage(Fatal, "bind errr, %d : %s\n", errno, strerror(errno));
            exit(Bind_Err);
        }

        ThreadPool<task_t>::GetInstance()->Start();
    }

    void AddOnlineUser(InetAddr addr)
    {
        LockGuard lockguard(&_user_mutex);
        for (auto &user : _online_user)
        {
            if (addr == user)
                return;
        }
        _online_user.push_back(addr);
        lg.LogMessage(Debug, "%s:%d is add to onlineuser list...\n", addr.Ip().c_str(), addr.Port());
    }
    void Route(int sock, const std::string &message)
    {
        LockGuard lockguard(&_user_mutex);
        for (auto &user : _online_user)
        {
            sendto(sock, message.c_str(), message.size(), 0, (struct sockaddr *)&user.GetAddr(), sizeof(user.GetAddr()));
            lg.LogMessage(Debug, "server send message to %s:%d, message: %s\n", user.Ip().c_str(), user.Port(), message.c_str());
        }
    }
    void Start()
    {
        // 服务器永远不退出
        char buffer[defaultsize];
        for (;;)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer); // 不能乱写
            ssize_t n = recvfrom(_sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &len);
            if (n > 0)
            {
                InetAddr addr(peer);
                AddOnlineUser(addr);
                buffer[n] = 0;
                std::string message = "[";
                message += addr.Ip();
                message += ":";
                message += std::to_string(addr.Port());
                message += "]# ";
                message += buffer;
                task_t task = std::bind(&UdpServer::Route, this, _sockfd, message);
                ThreadPool<task_t>::GetInstance()->Push(task);

                // 处理消息
                //  std::string response = _OnMessage(buffer);

                // std::cout << "[" << addr.PrintDebug() << "]# " << buffer << std::endl;
                // sendto(_sockfd, response.c_str(), response.size(), 0, (struct sockaddr *)&peer, len);
            }
        }
    }
    ~UdpServer()
    {
        pthread_mutex_destroy(&_user_mutex);
    }

private:
    // std::string _ip; // 后面要调整
    uint16_t _port;
    int _sockfd;

    std::vector<InetAddr> _online_user; // 会被多个线程同时访问的
    pthread_mutex_t _user_mutex;
    // cb_t _OnMessage;   // 回调
};