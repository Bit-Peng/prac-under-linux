#include "UdpSever.hpp"
#include "Comm.hpp"
#include <memory>
#include <vector>
#include <cstdio>

void Usage(std::string proc)
{
    cout<<"Usage:\n\t"<<proc<<"local_port\n"<<endl;
}

std::vector<string> black_words={
    "rm",
    "unlink",
    "cp",
    "mv",
    "chmid",
    "exit",
    "reboot",
    "halt",
    "shutdown",
    "top",
    "kill",
    "dd",
    "vim",
    "vi",
    "nano",
    "man"
};

string OnMessageDefault(string request)
{
    return request+"[haha,got you!!!]";
}

bool SafeCheck(string command)
{
    for(auto &k:black_words)
    {
        size_t pos=command.find(k);
        if(pos!=std::string::npos)
        {
            return false;
        }
    }
}

string ExecuteCommand(string command)
{
    if(!SafeCheck(command))
    {
        return "bad man!!!";
    }
    cout<<"get a message:"<<command<<endl;
    FILE*fp=popen(command.c_str(),"r");
    if(fp==nullptr)
    {
        return "execute error,reason is unknown";
    }
    string response;
    char buffer[1024];
    while(true)
    {
        char *s=fgets(buffer,sizeof(buffer),fp);
        if(!s) break;
        else response+=buffer;
    }
    pclose(fp);
    return response.empty()?"success":response;
}

int main(int argc,char *argv[])
{
    if(argc!=2)
    {
        Usage(argv[0]);
        return Usage_Err;
    }

    uint16_t port=stoi(argv[1]);
    std::unique_ptr<UdpServer> usvr=make_unique<UdpServer>(port);
    usvr->Init();
    usvr->Start();

    return 0;
}


